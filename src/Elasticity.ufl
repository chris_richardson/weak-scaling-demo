# Copyright (C) 2017 Chris N. Richardson and Garth N. Wells
#
# Licensed under the MIT License. See LICENSE file in the project root
# for full license information.

element = VectorElement("Lagrange", tetrahedron, 1)

u, v = TrialFunction(element), TestFunction(element)
f = Coefficient(element)

mu = Constant(tetrahedron)
lmbda = Constant(tetrahedron)

def epsilon(v):
    return 0.5*(grad(v) + grad(v).T)

def sigma(v, mu, lmbda):
    return 2.0*mu*epsilon(v) + lmbda*tr(epsilon(v))*Identity(v.geometric_dimension())

a = inner(sigma(u, mu, lmbda), epsilon(v))*dx
L = inner(f, v)*dx
