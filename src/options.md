# Recommended settings for elasticity

The following uses the conjugate gradient (CG) method with a smoothed
aggregation algebraic multigrid preconditioner (GAMG).

```
./dolfin-scaling-test \
--problem_type elasticity \
--scaling_type weak \
--ndofs 500000 \
--petsc.log_view \
--petsc.ksp_view \
--petsc.ksp_type cg \
--petsc.ksp_rtol 1.0e-8 \
--petsc.pc_type gamg \
--petsc.pc_gamg_coarse_eq_limit 1000 \
--petsc.mg_levels_ksp_type chebyshev \
--petsc.mg_levels_pc_type jacobi \
--petsc.mg_levels_esteig_ksp_type cg \
--petsc.matptap_via scalable \
--petsc.options_left
```

# Recommended settings for the Poisson equation

The following uses the conjugate gradient (CG) method with a classic
algebraic multigrid preconditioner (BoomerAMG).

```
./dolfin-scaling-test \
--problem_type poisson \
--scaling_type weak \
--ndofs 500000 \
--petsc.log_view \
--petsc.ksp_view \
--petsc.ksp_type cg \
--petsc.ksp_rtol 1.0e-8 \
--petsc.pc_type hypre \
--petsc.pc_hypre_type boomeramg \
--petsc.pc_hypre_boomeramg_strong_threshold 0.5 \
--petsc.options_left
```
